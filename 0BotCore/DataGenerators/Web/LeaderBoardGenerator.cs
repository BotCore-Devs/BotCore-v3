﻿using BotCore.Structures.Base;

using CoreBot.Structures.DataStorage;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace BotCore.DataGenerators.Web
{
    public class LeaderboardGenerator
    {
        private static Stopwatch _timer;
        private static string _basename;
        private static readonly string Basepage = @"<meta charset='UTF-8'><style>
  .pb {
    height: 25px;
    background-color: black;
    border-bottom-right-radius: 25px;
    border-bottom-left-radius: 25px;
    border-radius: 25px;
}
.pb > span {
    display: block;
    height: 100%;
    border-bottom-right-radius: 25px;
    border-bottom-left-radius: 25px;
border-radius: 25px;
    background-color: blue;
    position: relative;
}
body {background-color:#242424; color:#ffffff; font-family:'Segoe UI';}
rank {font-size:67.5px; color:#7d26cd; line-height:48px; vertical-align:middle;}
xp {font-size:12; vertical-align:middle; color:#a2a2a2}
name {font-size:20; vertical-align:middle;}
level {font-size:20; vertical-align:middle;}
discrim {font-size:10; vertical-align:middle}
user {width:500px; display:inline-block; background-color:#a2a2a20f; border-radius:25px; padding-top:20px; margin:5px;}
#avatar {height:64px; border-radius: 50%; vertical-align: middle; }
#userpanel {width:640px;height:480px;
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    border-radius: 25px;
    border: none;
    animation-name: ModalOpen;
    animation-delay:100ms; 
    animation-duration: 500ms;
    }
    #modal-bg {width:100%;height:100%;background-color:#000c;position:fixed;top:0px;left:0px;animation-name:ModalOpen2;animation-duration:500ms;}
@keyframes ModalOpen {
    from {width:0px;height:0px;opacity:0;}
    to {width:640px;height:480px;opacity:1;}
    } 
@keyframes ModalOpen2 {
    from {opacity:0;}
    to {opacity:1;}
    }
.leaveicon {
    width: 32px;
    vertical-align:middle;
    border-radius: 16px;
    }
</style>
<script src='https://code.jquery.com/jquery-3.5.1.min.js'></script>
<script>
function PrepareModal() {
    $('body').append(`<div id='modal-bg'></div>`);
    $('#modal-bg').click(CloseModal);
}
function CloseModal() {
    $('.modal').animate({
        opacity: 0,
        width: 0,
        height: 0
    }, 500, function()
        {
        $('.modal').remove();
        });
    $('#modal-bg').animate({
        opacity: 0
    }, 500, function()
        {
        $('#modal-bg').remove();
        });
}

    var loc = window.location.href;
    if(!loc.endsWith('/')) loc+='/';
    function OpenModal(id)
    {
        PrepareModal();
    $('#modal-bg').append(`<iframe src='${loc+id}.html' id='userpanel' class='modal'></iframe>`);
}
window.addEventListener('error', function(e) {
var uri = loc + e.target.accessKey + '/UpdateAvatar';
console.log(uri);
    $.get(uri, function (data, textStatus, jqXHR) { 
    e.target.outerHTML = data;
})
}, true);</script>
<meta charset='UTF-8'>
<center>
    <div>";
        public string Generate(ulong guildId)
        {
            _basename = "GenLeaderboard_" + guildId + "_" + DateTime.Now.Ticks;
            _timer = Bot.Timing.StartTiming(_basename);
            LegacyServer server = Bot.DataStore.GetServer(guildId).Result;
            List<LegacyUser> susers = new List<LegacyUser>();
            susers = server.EnumUsers().Where(x => x.XpLevel >= server.MinLevelForLeaderboard).ToList();
            susers.Sort(delegate (LegacyUser y, LegacyUser x)
            {
                int xdiff = x.XpLevel.CompareTo(y.XpLevel);
                if (xdiff != 0)
                {
                    return xdiff;
                }
                else
                {
                    return x.Xp.CompareTo(y.Xp);
                }
            });

            string scoreboard = @"";
            scoreboard = Basepage.Replace(".html", "");

            foreach (LegacyUser user in susers)
            {
                scoreboard += GetUserCard(user, susers.IndexOf(user) + 1);
            }
            scoreboard += $"\n</div>\n    </center>";

            Bot.Timing.StopTiming(_timer);
            return scoreboard;
        }

        private static readonly string UserTemplate = $@"
        <user onclick='OpenModal(`$ID`)'>
            <rank>$RANK</rank>
            <img id='avatar' loading='lazy' async accessKey='$ID' src='$PICURL'>
            <name$LEFT>$NAME<discrim>#$DISCRIM</discrim></name><br>
            <level>$LEVEL</level>
            <xp>($XPC/$XPG XP)</xp><br>
            <div class='pb'>
                <span style='width: $XPP%;'></span>
            </div>
        </user>";

        public LeaderboardGenerator(Bot bot)
        {
            Bot = bot;
        }

        private static Bot Bot { get; set; }

        public static string GetUserCard(LegacyUser user, int index)
        {
            if (!user.Changed && user.CachedLeaderboardIndex == index)
            {
                return user.CachedLeaderboardCard;
            }

            string a = UserTemplate
                .Replace("$PICURL", user.Left ? "https://thearcanebrony.net/bots/placeholder.png" : user.AvatarUrl)
                .Replace("$RANK", index + "")
                .Replace("$NAME", user.Username + "")
                .Replace("$DISCRIM", user.Discriminator)
                .Replace("$LEVEL", user.XpLevel + "")
                .Replace("$XPC", user.Xp + "")
                .Replace("$XPG", user.XpGoal + "")
                .Replace("$XPP", Math.Round((double)user.Xp / user.XpGoal * 100, 1) + "")
                .Replace("$ID", user.UserId + "")
                .Replace("$LEFT", user.Left ? " style='color: #666;'" : "")
                .Replace("$ID", user.UserId + "");
            user.CachedLeaderboardIndex = index;
            user.CachedLeaderboardCard = a;
            user.Changed = false;
            return a;
        }
    }
}
