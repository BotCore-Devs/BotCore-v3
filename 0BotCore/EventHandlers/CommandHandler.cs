using BotCore.Structures.Base;

using CoreBot.EventHandlers.Commands;
using CoreBot.Structures.Command;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace CoreBot.EventHandlers
{
    public class CommandHandler
    {
        public void Init()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            bot.Timing.StartTiming("Command Setup");
            //Console.WriteLine("Setting up commands...");
            RegisterCommand("help",
                new CommandDefinition(CommandCategory.BuiltIn, "Help command", async (ce) =>
                {
                    string helpText = $"**{bot.Name}** help\nPrefix: {ce.Server.Prefix}\n\n";
                    foreach (CommandDefinition d in Commands.Values)
                    {
                        /*
                        if (
                        (d.Category != CommandCategory.Hidden)
                        (d.Category == CommandCategory.Owner && ce.e.Author.Id == 84022289024159744)
                        || (d.Category == CommandCategory.Moderation && ce.Author.IsStaff))
                        )*/
                        if (d.HasPermission(ce) && d.Category != CommandCategory.Hidden && !ce.Server.DisabledCommands.Contains(d.Name))
                        {
                            helpText += $"[{d.Category}] `{d.Name}`: {d.Description}{(d.Donator ? " (Donator)" : "")}\n";
                        }
                        if (helpText.Length >= 1900) { await ce.SendMessageAsync(helpText); helpText = ""; }
                    }
                    helpText += $"*This bot was written by **The Arcane Brony***";
#if SMALL
                        HelpText += "\n\n***THIS BOT IS RUNNING IN SMALL MODE, SOME FEATURES MAY NOT BE AVAILABLE!***";
#endif
                    await ce.SendMessageAsync(helpText);
                })
            );
            CgDeveloper.SetupCommands(bot);
            CgBase.SetupCommands(bot);
            CgOwner.SetupCommands(bot);
#if !SMALL
            CgEconomy.SetupCommands(bot);
            CgFun.SetupCommands(bot);
            //CGHardware.SetupCommands(bot);
            CgModeration.SetupCommands(bot);
            //CGMemes.SetupCommands(bot);
            CgLeveling.SetupCommands(bot);
            //CgMusic.SetupCommands(bot);
            
            CgTickets.Setupcommands(bot);
#else
            commands.Remove("usercount");
            commands.Remove("role");
#endif
            Commands = Commands.OrderBy(x => x.Value.Category).ToDictionary(pair => pair.Key, pair => pair.Value);
            //Console.WriteLine($"Finished setting up commands! (Registered {commands.Count} commands in {sw.ElapsedMilliseconds} ms)");
            bot.Timing.StopTiming("Command Setup");
            sw.Stop();
        }
        public void RegisterCommand(string name, CommandDefinition def)
        {
            if (!Commands.ContainsKey(name))
            {
                def.Name = name;
                Commands.Add(name, def);
                //Console.WriteLine($"Added command {"[" + def.Category.ToString() + ']',-12} {name,-16} ({def.Description})");
            }
            else
            {
                Console.WriteLine($"Command {name} already exists!");
            }
        }
        public async Task HandleCommandAsync(CommandEnvironment ce)
        {
            if (Commands.ContainsKey(ce.Command))
            {
                CommandDefinition cd = Commands[ce.Command];
                if (ce.Server.DisabledCommands.Contains(ce.Command) && ce.Author.UserId != 84022289024159744) { if(ce.Server.DisabledCommandMessage) await ce.SendMessageAsync("This command has been disabled!"); }
                else
                {
                    // cd.Action(ce);
                    if (cd.HasPermission(ce))
                    {
                        cd.Action(ce);
                    }
                    else
                    {
                        await ce.SendMessageAsync($"You do not have permission to run `{ce.Command}`, {ce.E.Author.Mention} 🚫");
                    }
                    // await ce.SendMessageAsync($"Commands have been disabled due to security issues! Thanks for understanding.");
                }
            }
            else if (ce.Server.UnknownCommandMessage)
            {
                await ce.SendMessageAsync($"Unknown command:\n\n\"{ce.Command}\"\n - {ce.E.Author.Mention} in {ce.E.Channel.Mention}");
            }
        }

        public Dictionary<string, CommandDefinition> Commands = new Dictionary<string, CommandDefinition>();
        private Bot bot;

        public CommandHandler(Bot bot)
        {
            this.bot = bot;
        }
    }
}
