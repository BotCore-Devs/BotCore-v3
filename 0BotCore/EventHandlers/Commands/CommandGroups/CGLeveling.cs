﻿using BotCore.Structures.Base;

using CoreBot.Structures.Command;
using CoreBot.Structures.DataStorage;

using DSharpPlus.Entities;

using System;
using System.Collections.Generic;

namespace CoreBot.EventHandlers.Commands
{
    internal class CgLeveling
    {

        public static void SetupCommands(Bot bot)
        {
            if (bot.Name == "Omnibot") return;
            bot.CommandHandler.RegisterCommand("profile",
                new CommandDefinition(CommandCategory.Leveling, "Display user profile")
                {
                    Action = async (ce) =>
                    {
                        DiscordUser user = ce.E.MentionedUsers.Count >= 1 ? ce.E.MentionedUsers[0] : ce.E.Author;
                        await ce.SendMessageAsync($"Profile for ***{ce.E.Guild.Members[user.Id]}***:\nLevel: {ce.Pinged.XpLevel}\nXP: {ce.Pinged.Xp} ({ce.Pinged.XpMultiplier}x)\nXP Goal: {ce.Pinged.XpGoal}{(ce.Pinged.IsPremium ? $"\n\n{ce.E.Guild.Members[user.Id].DisplayName} is a premium user!" : "")}");
                    }
                }
            );
            bot.CommandHandler.RegisterCommand("top",
                new CommandDefinition(CommandCategory.Fun, "Top members of the server")
                {
                    Action = async (ce) =>
                    {
                        string scoreboard = "";

                        List<LegacyUser> susers = ce.Server.EnumUsers();
                        susers.Sort(delegate (LegacyUser y, LegacyUser x)
                        {
                            int xdiff = x.XpLevel.CompareTo(y.XpLevel);
                            if (xdiff != 0)
                            {
                                return xdiff;
                            }
                            else
                            {
                                return x.Xp.CompareTo(y.Xp);
                            }
                        });
                        foreach (LegacyUser user in susers.GetRange(0, susers.Count < 5 ? susers.Count : 5))
                        {
                            try
                            {
                                scoreboard += $"{susers.IndexOf(user) + 1}. {(await ce.E.Guild.GetMemberAsync(user.UserId)).DisplayName}\n" +
                                              $"\tLevel: {user.XpLevel}, XP: {user.Xp}/{user.XpGoal}\n";
                            }
                            catch (Exception)
                            {
                                scoreboard += $"{susers.IndexOf(user) + 1}. <invalid user entry?>\n" +
                                              $"\tLevel: {user.XpLevel}, XP: {user.Xp}/{user.XpGoal}\n";
                            }
                        }
                        await ce.SendMessageAsync($"Top 5 users in *{ce.E.Guild.Name}*:\n\n```{scoreboard}```");
                    }
                }
            );
            bot.CommandHandler.RegisterCommand("lb",
                new CommandDefinition(CommandCategory.Leveling, "Leaderboards (WIP, might be broken!)")
                {
                    Action = async (ce) =>
                    {
                        await ce.E.Channel.TriggerTypingAsync();
                        await ce.SendMessageAsync($"Leaderboard: <{bot.LeaderboardGenerator.Generate(ce.Server.GuildId)}>");
                    }
                }
            );
        }
    }
}
