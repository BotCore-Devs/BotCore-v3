﻿using System;
using BotCore.Structures.Base;

using CoreBot.Structures.Command;

using DSharpPlus.Entities;
using Newtonsoft.Json;
using System.Linq;
using System.Reflection;
using DSharpPlus;
using DSharpPlus.Exceptions;

namespace CoreBot.EventHandlers.Commands
{
    internal class CgModeration
    {
        public static void SetupCommands(Bot bot)
        {
            if (bot.Name == "Omnibot") return;
            bot.CommandHandler.RegisterCommand("config",
            new CommandDefinition(CommandCategory.Moderation, "Configure settings for the server")
            {
                Action = async (ce) =>
                {
                    if(!ce.E.Channel.PermissionsFor(((DiscordMember) ce.E.MentionedUsers[0]))
                        .HasFlag(Permissions.ManageGuild))
                    {
                        await ce.SendMessageAsync("You need `Manage Server` to use this command!");
                        return;
                    }
                    if (ce.Args.Count != 0)
                    {
                        switch (ce.Args[0])
                        {
                            case "levels":
                                if (ce.Args.Count == 1)
                                {
                                    await ce.SendMessageAsync("Subcommands: toggle");
                                }
                                else
                                {
                                    switch (ce.Args[1])
                                    {
                                        case "toggle":
                                            await ce.SendMessageAsync($"Leveling has been {ce.Server.LevelsEnabled.Toggle()}!");
                                            break;
                                    }
                                }
                                break;
                            case "prefix":
                                if (ce.Args.Count != 1)
                                {
                                    await ce.SendMessageAsync($"Prefix changed: `{ce.Server.Prefix}` > `{ce.Server.Prefix = ce.Args[1]}`.");
                                }
                                else
                                {
                                    await ce.SendMessageAsync($"Current prefix: `{ce.Server.Prefix}`.");
                                }
                                break;
                            case "autorole":
                                await ce.SendMessageAsync($"Autorole is now {ce.Server.AutoRole.Toggle()}abled!");
                                break;
                            case "lockdown":
                                ce.Server.Lockdown = !ce.Server.Lockdown;
                                //Log.Mod($"Lockdown mode has been {(config.Lockdown ? "en" : "dis")}abled by {ce.e.Author.DisplayName()}");
                                await ce.SendMessageAsync($"Lockdown mode has been {(ce.Server.Lockdown ? "en" : "dis")}abled!");
                                break;
                            case "contentfilter":
                                if (ce.Args.Count > 1)
                                {
                                    switch (ce.Args[1])
                                    {
                                        case "toggle":
                                            ce.Server.ContentFilter.Enabled = !ce.Server.ContentFilter.Enabled;
                                            await ce.SendMessageAsync($"Content filter has been {(ce.Server.ContentFilter.Enabled ? "en" : "dis")}abled!");
                                            break;
                                        case "set":
                                            if (ce.Args.Count > 3)
                                            {
                                                PropertyInfo property = ce.Server.ContentFilter.GetType().GetProperty(ce.Args[2]);
                                                if (property == null)
                                                {
                                                    await ce.SendMessageAsync($"Could not find property (case sensitive!).\nCurrent config:```json\n{JsonConvert.SerializeObject(ce.Server.ContentFilter, Formatting.Indented)}```");
                                                }
                                                else
                                                {
                                                    switch (property.GetValue(ce.Server.ContentFilter))
                                                    {
                                                        case bool:
                                                            property.SetValue(ce.Server.ContentFilter, bool.Parse(ce.Args[3]));
                                                            break;
                                                        case int:
                                                            property.SetValue(ce.Server.ContentFilter, int.Parse(ce.Args[3]));
                                                            break;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                await ce.SendMessageAsync($"Current config:```json\n{JsonConvert.SerializeObject(ce.Server.ContentFilter, Formatting.Indented)}```");
                                            }

                                            break;
                                    }
                                }
                                else
                                {
                                    await ce.SendMessageAsync($"Current settings for content filter:```json\n{JsonConvert.SerializeObject(ce.Server.ContentFilter, Formatting.Indented)}```");
                                }

                                break;
                            case "wordfilter":
                                if (ce.Args.Count == 1)
                                {
                                    await ce.SendMessageAsync($"Banned words:\n - {string.Join("\n - ", ce.Server.WordFilter)}");
                                }
                                else if (ce.Args.Count == 2)
                                {

                                }
                                else
                                {
                                    string str = string.Join(" ", ce.Args.GetRange(2, ce.Args.Count - 2));
                                    switch (ce.Args[1])
                                    {
                                        case "add":

                                            ce.Server.WordFilter.Add(str);
                                            await ce.SendMessageAsync($"Added `{str}` to word filter!");
                                            break;
                                        case "remove":
                                            ce.Server.WordFilter.Remove(str);
                                            await ce.SendMessageAsync($"Removed `{str}` from word filter!");
                                            break;
                                        default:
                                            await ce.SendMessageAsync($"Unrecognised subcommand *{ce.Args[1]}*!");
                                            break;
                                    }
                                }
                                break;
                            case "namefilter":
                                if (ce.Args.Count == 1)
                                {
                                    await ce.SendMessageAsync($"Banned name parts:\n - {string.Join("\n - ", ce.Server.BannedNameParts)}");
                                }
                                else if (ce.Args.Count == 2)
                                {

                                }
                                else
                                {
                                    string str = string.Join(" ", ce.Args.GetRange(2, ce.Args.Count - 2));
                                    switch (ce.Args[1])
                                    {
                                        case "add":
                                            ce.Server.BannedNameParts.Add(str);
                                            await ce.SendMessageAsync($"Added `{str}` to name filter!");
                                            break;
                                        case "remove":
                                            ce.Server.BannedNameParts.Remove(str);
                                            await ce.SendMessageAsync($"Removed `{str}` from name filter!");
                                            break;
                                        default:
                                            await ce.SendMessageAsync($"Unrecognised subcommand *{ce.Args[1]}*!");
                                            break;
                                    }
                                }
                                break;
                            case "ignoredchannels":
                                if (ce.Args.Count == 1)
                                {
                                    await ce.SendMessageAsync($"IgnoredChannels:\n - {string.Join("\n - ", ce.Server.IgnoredChannels)}");
                                }
                                else if (ce.Args.Count == 2)
                                {

                                }
                                else
                                {

                                    //string str = string.Join(" ", ce.Args.GetRange(2, ce.Args.Count - 2));
                                    switch (ce.Args[1])
                                    {
                                        case "add":
                                            foreach (DiscordChannel ch in ce.E.Message.MentionedChannels)
                                            {
                                                ce.Server.IgnoredChannels.Add(ch.Id);
                                            }
                                            await ce.SendMessageAsync($"Added to ignored channel list!");
                                            break;
                                        case "remove":
                                            foreach (DiscordChannel ch in ce.E.Message.MentionedChannels)
                                            {
                                                if (ce.Server.IgnoredChannels.Contains(ch.Id))
                                                {
                                                    ce.Server.IgnoredChannels.Remove(ch.Id);
                                                }
                                            }
                                            await ce.SendMessageAsync($"Removed from ignored channel list!");
                                            break;
                                        default:
                                            await ce.SendMessageAsync($"Unrecognised subcommand *{ce.Args[1]}*!");
                                            break;
                                    }
                                    ce.Server.IgnoredChannels = ce.Server.IgnoredChannels.Distinct().ToList();
                                }
                                break;
                            }
                    }
                    else
                    {
                        await ce.SendMessageAsync("Subcommands: prefix, autorole, wordfilter, namefilter, ignoredchannels, contentfilter");
                    }
                }
            }
        );
            bot.CommandHandler.RegisterCommand("mute",
                new CommandDefinition(CommandCategory.Moderation, "Mute a user for a set amount of time")
                {
                    Action = async (ce) =>
                    {
                        //code
                        if (false && !Checks.UserAffectableByModeration(ce))
                        {
                            await ce.SendMessageAsync("This user can't be muted, due to safety restrictions.");
                            return;
                        }
                        if (ce.Args.Count == 1)
                        {
                            if (ce.E.Channel.PermissionsFor(((DiscordMember) ce.E.MentionedUsers[0]))
                                .HasFlag(Permissions.ManageMessages))
                            {
                                if (((DiscordMember) ce.E.MentionedUsers[0]).Hierarchy <
                                    ((DiscordMember) ce.E.Author).Hierarchy)
                                {
                                    
                                    //ce.E.Guild.GetRole(589673360242376716)
                                    try
                                    {
                                        await ((DiscordMember) ce.E.MentionedUsers[0]).GrantRoleAsync(ce.E.Guild.Roles
                                            .First(x => x.Value.Name == "Muted").Value);
                                    }
                                    catch (InvalidOperationException)
                                    {
                                        await ce.SendMessageAsync("No `Muted` role found!");
                                    }
                                    catch (UnauthorizedException)
                                    {
                                        await ce.SendMessageAsync("Bot does not have permission to manage roles!");
                                    }
                                }
                                ce.Pinged.Muted = true;
                                await ce.SendMessageAsync("User has been successfully muted!"); // success
                                // Log.Mod($"{ce.e.Author.DisplayName()} has muted {ce.e.MentionedUsers[0].DisplayName()} indefinitely.");
                            }
                            else
                            {
                                await ce.SendMessageAsync("You do not have permission to use this command!");
                            }
                        }
                        else if (ce.Args.Count >= 2)
                        {

                            ulong time = 0;
                            if ((time = Util.ParseTime(ce.Args[1])) > 0)
                            {
                                ce.Pinged.Muted = true;
                                ce.Pinged.Mute();
                                await ce.SendMessageAsync($"User has been successfully muted until {ce.Pinged.UnmuteBy}!"); // success
                                //Log.Mod($"{ce.e.Author.DisplayName()} has muted {ce.e.MentionedUsers[0].DisplayName()} until {ce.Pinged.UnmuteBy}!");
                            }
                            else
                            {
                                await ce.SendMessageAsync($"You have entered an invalid time string! ({time} seconds)"); // success
                            }
                        }
                        else
                        {
                            await ce.SendMessageAsync("You must ping a member!"); // no member ce.Pinged
                        }
                    }
                }
            );

            bot.CommandHandler.RegisterCommand("unmute",
                new CommandDefinition(CommandCategory.Moderation, "Unmute user")
                {
                    Action = async (ce) =>
                    {
                        if(!ce.E.Channel.PermissionsFor(((DiscordMember) ce.E.MentionedUsers[0]))
                            .HasFlag(Permissions.ManageMessages))
                        {
                            await ce.SendMessageAsync("You need `Manage Messages` to use this command!");
                            return;
                        }
                        //code
                        if (ce.Args.Count >= 1)
                        {
                            ce.Pinged.Unmute();
                            await ce.SendMessageAsync("User has been successfully unmuted!");
                        }
                        else
                        {
                            await ce.SendMessageAsync("You must ping a member!");
                        }
                    }
                }
            );
            bot.CommandHandler.RegisterCommand("slowmode",
                new CommandDefinition(CommandCategory.Moderation, "Set a custom slowmode time")
                {
                    Action = async (ce) =>
                    {
                        if(!ce.E.Channel.PermissionsFor(((DiscordMember) ce.E.MentionedUsers[0]))
                            .HasFlag(Permissions.ManageChannels))
                        {
                            await ce.SendMessageAsync("You need `Manage Channel` to use this command!");
                            return;
                        }
                        ulong seconds = Util.ParseTime(ce.Args[0]);
                        if (!(seconds > 0 || seconds < 21600))
                        {
                            await ce.SendMessageAsync($"Invalid number entered, you need to enter the time in seconds! (0 - 21600s/6h)\nYou entered: `{ce.Args[0]}` and it evaluated to {seconds} seconds."); // invalid number
                            return;
                        }
                        await ce.E.Channel.ModifyAsync(em => { em.PerUserRateLimit = (int)seconds; em.AuditLogReason = $"Rate limit set to {seconds} by {ce.E.Author.Username}#{ce.E.Author.Discriminator}."; });
                        await ce.SendMessageAsync($"Set channel slowmode to {seconds} seconds!");

                    }
                }
            );
            bot.CommandHandler.RegisterCommand("prune",
                new CommandDefinition(CommandCategory.Moderation, "Prune an amount of messages")
                {
                    Action = async (ce) =>
                    {
                        if(!ce.E.Channel.PermissionsFor(((DiscordMember) ce.E.MentionedUsers[0]))
                            .HasFlag(Permissions.ManageMessages))
                        {
                            await ce.SendMessageAsync("You need `Manage Messages` to use this command!");
                            return;
                        }
                        if (!int.TryParse(ce.Args[0], out int count))
                        {
                            await ce.SendMessageAsync("Invalid number entered, you need to enter the number of messages!"); // invalid number
                            return;
                        }
                        await ce.E.Channel.DeleteMessagesAsync(await ce.E.Channel.GetMessagesAsync(count + 1), $"{count} messages pruned by {ce.E.Author.Username}#{ce.E.Author.Discriminator}");
                        await ce.SendMessageAsync($"Pruned {count} messages!");
                    }
                }
            );
            bot.CommandHandler.RegisterCommand("warn",
                new CommandDefinition(CommandCategory.Moderation, "Warn a user")
                {
                    Action = async (ce) =>
                    {
                        if(!ce.E.Channel.PermissionsFor(((DiscordMember) ce.E.MentionedUsers[0]))
                            .HasFlag(Permissions.ManageMessages))
                        {
                            await ce.SendMessageAsync("You need `Manage Messages` to use this command!");
                            return;
                        }
                        if (ce.Args.Count >= 2)
                        {
                            ce.Args.RemoveAt(0);
                            ce.Pinged.Warnings.Add(string.Join(" ", ce.Args));
                            await ce.SendMessageAsync($"Warned {ce.E.Guild.Members[ce.E.MentionedUsers[0].Id].DisplayName()} (#{ce.Pinged.Warnings.Count}: {string.Join(" ", ce.Args)})");
                        }
                        else
                        {
                            await ce.SendMessageAsync($"You must enter a reason");
                        }
                    }
                }
            );
            bot.CommandHandler.RegisterCommand("disablecmd",
                new CommandDefinition(CommandCategory.Moderation, "Disable a command")
                {
                    Action = async (ce) =>
                    {
                        if(!ce.E.Channel.PermissionsFor(((DiscordMember) ce.E.MentionedUsers[0]))
                            .HasFlag(Permissions.ManageGuild))
                        {
                            await ce.SendMessageAsync("You need `Manage Server` to use this command!");
                            return;
                        }
                        if (ce.Args.Count >= 1)
                        {
                            foreach (string cmd in ce.Args)
                            {
                                ce.Server.DisabledCommands.Add(cmd);
                            }
                        }
                        else
                        {
                            await ce.SendMessageAsync($"You must enter a command");
                        }
                    }
                }
            );
            bot.CommandHandler.RegisterCommand("warns",
                new CommandDefinition(CommandCategory.Moderation, "List the warnings for a user")
                {
                    Action = async (ce) =>
                    {
                        await ce.SendMessageAsync($"Warns for {ce.E.MentionedUsers[0]}:\n```  - {string.Join(".\n  - ", ce.Pinged.Warnings)}```"); // no member ce.Pinged
                    }
                }
            );
            bot.CommandHandler.RegisterCommand("removewarn",
                new CommandDefinition(CommandCategory.Moderation, "Remove a warning from a user")
                {
                    Action = async (ce) =>
                    {
                        if(!ce.E.Channel.PermissionsFor(((DiscordMember) ce.E.MentionedUsers[0]))
                            .HasFlag(Permissions.ManageMessages))
                        {
                            await ce.SendMessageAsync("You need `Manage Messages` to use this command!");
                            return;
                        }
                        if (int.TryParse(ce.Args[1], out int num))
                        {
                            ce.Pinged.Warnings.RemoveAt(num);
                            await ce.SendMessageAsync($"Warning removed!");
                        }
                        else
                        {
                            await ce.SendMessageAsync($"Enter a correct number!");
                        }
                    }
                }
            );
        }
    }
}
