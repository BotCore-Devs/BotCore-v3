﻿using System;
using System.Linq;
using BotCore.Structures.Base;
using CoreBot.Structures.Command;
using DSharpPlus;
using DSharpPlus.Entities;
using Omnibot.Structures.LegacyDataStorage;

namespace CoreBot.EventHandlers.Commands
{
    public class CgTickets
    {
        public static void Setupcommands(Bot bot)
        {
            bot.CommandHandler.RegisterCommand("ticket", 
                new CommandDefinition(CommandCategory.Ticket,
                    "Open/manage tickets", async (ce) =>
                    {
                        switch (ce.Args.Count>0?ce.Args[0]:"")
                        {
                            case "open":
                                if (ce.Server.Tickets.Any(x => x.OwnerId == ce.E.Author.Id && x.IsOpen))
                                {
                                    await ce.SendMessageAsync("You already own a ticket in this server! You must close it first!");
                                } else
                                {
                                    DiscordChannel newlyOpenedChannel =
                                        (await ce.E.Guild.CreateChannelAsync(
                                            $"{ce.Server.Tickets.Count}-{ce.E.Author.Username.ToAlphaNumeric()}",
                                            ChannelType.Text, ce.E.Guild.Channels[747981696884015147]));
                                    ce.Server.Tickets.Add(new Ticket()
                                    {
                                        OwnerId = ce.E.Author.Id,
                                        ChannelId = newlyOpenedChannel.Id
                                    });
                                    await ce.SendMessageAsync(
                                        $"New ticket opened! {newlyOpenedChannel.Mention}");
                                }

                                ce.Server.Save();
                                break;
                            case "close":
                                Ticket ticketToClose = ce.Server.Tickets.FirstOrDefault(x => x.OwnerId == ce.E.Author.Id && x.IsOpen);
                                if (ticketToClose == null)
                                {
                                    await ce.SendMessageAsync("You don't have any open tickets!");
                                }
                                else
                                {
                                    
                                    ce.Server.Tickets[ce.Server.Tickets.IndexOf(ticketToClose)].IsOpen = false;
                                    ticketToClose.IsOpen = false;
                                    ce.Server.Save();
                                    Console.WriteLine(ticketToClose.IsOpen);
                                    DiscordChannel channelToClose = ce.E.Guild.Channels[ticketToClose.ChannelId];
                                    await channelToClose.ModifyAsync((model =>
                                    {
                                        model.Name = 'c' + channelToClose.Name;
                                        model.AuditLogReason = "Closed ticket";
                                    }));
                                    await channelToClose.AddOverwriteAsync(ce.E.Guild.EveryoneRole,
                                        deny: Permissions.AccessChannels, reason: "Closed ticket");
                                    
                                }
                                break;
                            case "delete":
                                if (!Checks.IsStaff(ce))
                                {
                                    await ce.SendMessageAsync("You must be staff to use this command!");
                                    return;
                                }
                                if (ce.Args.Count == 1)
                                {
                                    await ce.SendMessageAsync("You must provide a channel ID");
                                    return;
                                }
                                Ticket ticketToDelete = ce.Server.Tickets.FirstOrDefault(x => x.ChannelId + "" == ce.Args[1]);
                                if (ticketToDelete == null)
                                {
                                    await ce.SendMessageAsync("Ticket doesnt exist??");
                                }
                                else
                                {
                                    ce.Server.Tickets.Remove(ticketToDelete);
                                    ce.Server.Save();
                                    DiscordChannel channelToDelete = ce.E.Guild.Channels[ticketToDelete.ChannelId];
                                    await channelToDelete.DeleteAsync("Ticket deleted: " + channelToDelete.Name);
                                    
                                }
                                break;
                            default:
                                await ce.SendMessageAsync("Ticket system:\n\nAvailable subcommands:\n- open\n- close\n- delete <channel id> (admin only)");
                                break;
                        }
                    }));
        }
    }
}