﻿using BotCore.Structures.Base;

using CoreBot.Structures.DataStorage;

using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreBot.EventHandlers
{
    public class GuildMemberAddedHandler
    {
        private Bot bot;

        public GuildMemberAddedHandler(Bot bot)
        {
            this.bot = bot;
        }

        private readonly List<DateTime> lastJoins = new();
        private readonly List<double> offsets = new();
        public async Task HandleNewMember(DiscordClient client, GuildMemberAddEventArgs e) //on member join
        {
            if (e.Guild.Id == 391478785754791949)
            {
                lastJoins.Add(e.Member.JoinedAt.LocalDateTime);
                if(lastJoins.Count > 6) lastJoins.RemoveAt(0);
                offsets.Clear();
                for (int i = 1; i < lastJoins.Count; i++)
                {
                    offsets.Add(lastJoins[i].Subtract(lastJoins[i - 1]).TotalSeconds);
                }

                if (offsets.Count >= 2 && offsets.Average() <= 30)
                {
                    await e.Member.SendMessageAsync(":smiling_imp: Heaven's closed!\nIf you think this is in error, please contact `The Arcane Brony#9669`!");
                    await e.Member.RemoveAsync($"Anti-raid: average offset = {offsets.Average()}");
                }
            }
            
            LegacyServer server = await bot.DataStore.GetServer(e.Guild.Id);
            LegacyUser user = server.GetUser(e.Member.Id, false);
            if (e.Guild.Channels.Values.ToList()[0].UserHasPermission(Permissions.ManageGuild, e.Guild.CurrentMember))
            {
                try
                {
                    foreach (DiscordInvite invite in await e.Guild.GetInvitesAsync())
                    {
                        if (server.Invites.ContainsKey(invite.Code) && invite.Uses > server.Invites[invite.Code].Uses)
                        {
                            Console.WriteLine($"Invite {invite.Code} ({invite.Inviter}) was used by {e.Member}");
                            server.Invites[invite.Code] = invite;
                            user.InvitesUsed.Add(invite.Code);
                        }
                        else
                        {
                            server.Invites.TryAdd(invite.Code, invite);
                        }
                    }
                }
                catch
                {
                    user.InvitesUsed.Add("403 Access Denied");
                }
            }
            else
            {
                user.InvitesUsed.Add("Bot was missing permissions");
            }

            if (e.Member.Username.ToLower().ContainsAnyOf(server.BannedNameParts) || server.Lockdown)
            {
                server.GetUser(e.Member.Id, false).Mute();
            }
            if (server.AutoRole && server.Roles.Member != 0) //autorole
            {
                await e.Member.GrantRoleAsync(e.Guild.GetRole(server.Roles.Member));
            }
        }
    }
}
