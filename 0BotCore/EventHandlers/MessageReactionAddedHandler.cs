﻿using BotCore.Structures.Base;

using CoreBot.Structures.DataStorage;

using DSharpPlus;
using DSharpPlus.EventArgs;

using System.Linq;
using System.Threading.Tasks;

namespace CoreBot.EventHandlers
{
    public class MessageReactionAddedHandler
    {
        private Bot bot;

        public MessageReactionAddedHandler(Bot bot)
        {
            this.bot = bot;
        }

        public async Task HandleReaction(DiscordClient client, MessageReactionAddEventArgs e)
        {
            //reaction role code from anonbot, for later use
            /*if (e.Channel.Id == 718453038460960790)
            {
                DiscordMessage Message = await e.Channel.GetMessageAsync(e.Message.Id);
                String[] lines = Message.Content.Split('\n');
                foreach (string line in lines)
                {
                    string emote = line.Split(' ')[0];
                    string rolename = line.Split(' ', 2)[1];
                    foreach (KeyValuePair<ulong, DiscordRole> role in e.Guild.Roles.ToList())
                    {
                        if (role.Value.Name == rolename)
                        {
                            Console.WriteLine("Found role: " + rolename);
                            await e.User.Member().GrantRoleAsync(e.Guild.GetRole(role.Key), "Self assigned role");
                            break;
                        }
                    }

                }
            }*/
            /* If the emote/reaction added to the message is a star emoji */
            if (e.Emoji.GetDiscordName() == ":star:")
            {
                LegacyUser user = (await bot.DataStore.GetServer(e.Guild.Id)).GetUser((await e.Channel.GetMessageAsync(e.Message.Id)).Author.Id, false);
                QuotedMessage quote = new QuotedMessage
                {
                    ServerId = e.Guild.Id,
                    ChannelId = e.Channel.Id,
                    MessageId = e.Message.Id,
                    Text = (await e.Channel.GetMessageAsync(e.Message.Id)).Content,
                    Rating = 1
                };
                foreach (QuotedMessage aquote in user.Quotes.FindAll(x => { return x.ServerId == e.Guild.Id && x.ChannelId == e.Channel.Id && x.MessageId == e.Message.Id; }))
                {
                    quote.Rating += aquote.Rating;
                }
                user.Quotes.RemoveAll(x => { return x != quote && x.MessageId == e.Message.Id; });
                user.Quotes.Add(quote);
                user.Quotes = user.Quotes.OrderByDescending(x => x.Rating).ToList();

                user.Save();
            }
        }
    }
}
