﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading;
using BotCore.WebServices_ASP;

namespace BotCore.Structures.Base
{
    class BotManager
    {
        //generic shared data
        public static bool IsLinux => RuntimeInformation.IsOSPlatform(OSPlatform.Linux);
        public static string DefaultToken = "NzQ3MzE4MjE2NTkzODk5NTUx.X0NIUw." + "-eGUHAHiILQwSkSQWompY-BjlDI";

        //bot manager
        public static Dictionary<string, Bot> Bots = new();
        public static bool IsSingleBot { get => Bots.Count == 1; }
        private static int _lastPort = 81;
        private static bool _isAnyBotCheckingPorts;
        public static int GetNextPort
        {
            get
            {
                int CurrentPort = 81;
                if (IsSingleBot && WebProvider.PortFree(CurrentPort)) return CurrentPort;
                bool NoBotsUsingThisPort=Bots.All(x => x.Value.Port != CurrentPort);
                bool PortFree=WebProvider.PortFree(CurrentPort);
                while (!((NoBotsUsingThisPort=Bots.All(x => x.Value.Port != CurrentPort)) && (PortFree=WebProvider.PortFree(CurrentPort))))
                {
                    if(Bots.Count>0 && Bots.Any(x => x.Value.Port == CurrentPort)) Console.WriteLine($"Found {Bots.First(x => x.Value.Port == CurrentPort).Key} at port {CurrentPort}");
                    CurrentPort++;
                }
                Console.WriteLine($"Using port {CurrentPort}");
                return CurrentPort;
                
                while(_isAnyBotCheckingPorts) Thread.Sleep(10);
                _isAnyBotCheckingPorts = true;
                using TcpClient tcpClient = new TcpClient();
                while (true)
                {
                    try
                    {
                        Console.WriteLine("Testing port " + _lastPort);
                        if (_lastPort != 87)
                        {
                            tcpClient.Connect("127.0.0.1", _lastPort);
                            tcpClient.Close();
                            tcpClient.Dispose();
                        }
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Found free port " + _lastPort);
                        _isAnyBotCheckingPorts = false;
                        return _lastPort++;
                    }
                    _lastPort++;
                }
            }
        }
    }
    public enum Bots
    {
        SiliconBot,
        Omnibot,
        SiliconBotPublic,
        AnonBot,
        KinoBot,
        HericanBot,
        Playground,
        StudioBot
    }
}
