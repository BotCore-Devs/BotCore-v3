﻿namespace SiliconBotCore3
{
    public partial class Level
    {
        public long? UserId { get; set; }
        public long? Xp { get; set; }
        public long? Level1 { get; set; }
        public int? Multiplier { get; set; }
        public long DServerId { get; set; }
        public string LevelId { get; set; }

        public virtual GlobalUser User { get; set; }
    }
}
