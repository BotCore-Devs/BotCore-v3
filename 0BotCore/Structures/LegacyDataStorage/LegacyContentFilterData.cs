﻿using Newtonsoft.Json;
using System.Threading;

namespace CoreBot.Structures.DataStorage
{
    public class LegacyContentFilterData
    {
        public double Emotes;
        public double Invites;
        public double Mentions;
        public double BadLang;
        [JsonIgnore]
        public double Total { get => Emotes + Invites + Mentions + BadLang; }
        [JsonIgnore]
        public Thread UserContentFilterThread = new Thread(() => { });
    }
}
