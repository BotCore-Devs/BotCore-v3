﻿using System;
using System.Collections.Generic;
using System.Linq;
using BotCore.Structures.Base;
using CoreBot.EventHandlers.Commands;
using CoreBot.Structures.Command;
using DSharpPlus;
using DSharpPlus.Entities;
using Omnibot.EventHandlers;

namespace BotCore.Structures.ServiceWorkers
{
    public class ImpulsYeeterBehaviors
    {
        private Bot bot;

        //guild
        private DiscordGuild server = null;

        //voice channels
        DiscordChannel TimeoutRoom = null, Cantine = null, TeachersRoom = null;

        //text channels
        DiscordChannel LogChannel = null, GeneralChannel = null;

        public ImpulsYeeterBehaviors(Bot _bot)
        {
            bot = _bot;
        }

        public void RegisterCommands()
        {
            bot.CommandHandler.RegisterCommand("yeet", new CommandDefinition(CommandCategory.Moderation,
                "Kick a student out of class.", async ce =>
                {
                    if(!ce.E.Channel.PermissionsFor(((DiscordMember) ce.E.Author))
                        .HasFlag(Permissions.ManageMessages))
                    {
                        await ce.SendMessageAsync("You need Manage Messages to use this command!");
                        return;
                    }
                    if (server == null) server = bot.Client.Guilds[803265327768141834];
                    if (GeneralChannel == null) GeneralChannel = server.Channels[803265328255467593];
                    if (LogChannel == null) LogChannel = server.Channels[803297237701623868];
                    if (TimeoutRoom == null) TimeoutRoom = ce.E.Guild.Channels[803288526752841819];
                    if (ce.E.MentionedUsers.Count < 1)
                    {
                        await ce.SendMessageAsync("You must mention at least one user!");
                    }
                    else
                    {
                        DiscordMessage msg = null;
                        IReadOnlyCollection<DiscordMember> members = await ce.E.Guild.GetAllMembersAsync();
                        foreach (var user in ce.E.MentionedUsers)
                        {
                            if (members.First(x => x.Id == user.Id).Roles.Any(x => x.Id == 803266147847110696))
                            {
                                if (msg == null)
                                    msg = await ce.SendMessageAsync(
                                        $"Kicking students out of class {members.First(x => x.Id == ce.E.Author.Id).VoiceState.Channel.Name}...");
                                msg = await msg.ModifyAsync($"{msg.Content}\nSkipped {user}...");
                            }
                            else
                            {
                                await (members.First(x => x.Id == user.Id)).PlaceInAsync(TimeoutRoom);
                                if (msg == null)
                                    msg = await ce.SendMessageAsync(
                                        $"Kicking students out of class {members.First(x => x.Id == ce.E.Author.Id).VoiceState.Channel.Name}...");
                                msg = await msg.ModifyAsync($"{msg.Content}\nKicked {user}...");
                            }
                        }

                    }

                }
            ));
            bot.CommandHandler.RegisterCommand("break", new CommandDefinition(CommandCategory.Moderation, "Break time!",
                async ce =>
                {
                    if(!ce.E.Channel.PermissionsFor(((DiscordMember) ce.E.Author))
                        .HasFlag(Permissions.ManageMessages))
                    {
                        await ce.SendMessageAsync("You need Manage Messages to use this command!");
                        return;
                    }
                    BreakTime(ce.E.Author.ToString());
                }
            ));
            bot.TimedEventManager.StartTimedEvent(() =>
            {
                BreakTime();
            }, "Noon break", DateTime.Parse("12:00:00").TimeOfDay, TimeSpan.FromHours(24));
        }

        public async void BreakTime(string reason = "Auto")
        {
            if (server == null) server = bot.Client.Guilds[803265327768141834];
            if (GeneralChannel == null) GeneralChannel = server.Channels[803265328255467593];
            if (LogChannel == null) LogChannel = server.Channels[803297237701623868];
            if (Cantine == null) Cantine = server.Channels[803290103060365353];
            if (TeachersRoom == null) TeachersRoom = server.Channels[803290280765685780];
            await LogChannel.SendMessageAsync($"{reason} triggered break time");
            DiscordMessage msg = null;
            IReadOnlyCollection<DiscordMember> members = await server.GetAllMembersAsync();
            List<DiscordUser> usersInVoice = new();
            foreach (var channel in server.Channels)
            {
                if (channel.Value.Type == ChannelType.Voice)
                {
                    usersInVoice.AddRange(channel.Value.Users);
                }
            }

            foreach (var user in usersInVoice)
            {
                await (members.First(x => x.Id == user.Id)).PlaceInAsync(
                    members.First(x => x.Id == user.Id).Roles.Any(x => x.Id == 803266147847110696)
                        ? TeachersRoom
                        : Cantine);
                if (msg == null)
                    msg = await GeneralChannel.SendMessageAsync($"It is break time!", true);
                msg = await msg.ModifyAsync(
                    $"It is break time!\n{Cantine.Users.Count()} students and {TeachersRoom.Users.Count()} teachers moved...");
            }
        }
    }
}