﻿using System.Threading.Tasks;
using BotCore.Structures.Base;
using CoreBot.Structures.DataStorage;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NJsonSchema;

namespace BotCore.WebServices
{
    internal class JsonHtmlFormProvider
    {
        public static async Task<string> GetServerForm(Bot bot, ulong guildId)
        {
            LegacyServer server = await bot.DataStore.GetServer(guildId);
            JObject clean = JObject.FromObject(server);

            clean.Remove("Invites");
            clean.Remove("GuildID");
            clean.Remove("MessageCount");
            JsonSchema schema = JsonSchema.FromSampleJson(JsonConvert.SerializeObject(clean));

            return @"<html lang='en'>
    <meta charset='utf-8'>
    <title>JSON Editor</title>
    <style>
        .json-editor-btntype-properties {display:none !important;}
        .form-control {height: 32px !important;}
        textarea.form-control {height: auto !important;}
    </style>
    <script src='https://cdn.jsdelivr.net/npm/@json-editor/json-editor@latest/dist/jsoneditor.min.js'></script>
    <script src='https://cdn.jsdelivr.net/npm/lz-string@1.4.4/libs/lz-string.min.js'></script>
    <link rel='stylesheet' id='theme-link' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css'>
    <link rel='stylesheet' id='iconlib-link' href='https://use.fontawesome.com/releases/v5.6.1/css/all.css'>
</head>
<body>
<div class='container grid-xl'>
    <div class='row columns md:flex'>
        <div class='col-8 col-md-8 w-8/12'>
            <h1>Editor</h1>
            <p>Below is the editor generated from the JSON Schema.</p>
            <div id='json-editor-form'></div>
        </div>
        <div class='col-4 col-md-4 w-4/12'>
            <div>
                <a class='btn btn-primary' id='direct-link' title='preserves schema, value, and options'>Direct Link</a>
                <a class='btn btn-secondary' href='?' title='reload editor with default example settings'>reset</a>
            </div>
            <h2>JSON Output</h2>
            <label for='output-textarea'></label>
            <button class='btn btn-primary btn-block' id='setvalue'>Update Form</button>
            <textarea id='output-textarea' rows='15' style='width: 100%; font-family: monospace;' class='form-control'></textarea>
            <h2>Validation</h2>
            <textarea id='validate-textarea' readonly='' disabled='' class='form-control'></textarea>
            </div>
    </div>
</div>
<script>

  const defaultSchema = " + schema.ToJson() + @"

  let data = {}

  let defaultOptions = {
    iconlib: 'fontawesome5',
    object_layout: 'normal',
    schema: defaultSchema,
    show_errors: 'interaction',
    theme: 'bootstrap4'
  }

  let jsoneditor = null
  let directLink = document.querySelector('#direct-link')

  const booleanOptionsSelect = document.querySelector('#boolean-options-select')
  const head = document.getElementsByTagName('head')[0]
  const iconlibSelect = document.querySelector('#iconlib-select')
  const iconlibLink = document.querySelector('#iconlib-link')
  const libSelect = document.querySelector('#lib-select')
  const jsonEditorForm = document.querySelector('#json-editor-form')
  const objectLayoutSelect = document.querySelector('#object-layout-select')
  const outputTextarea = document.querySelector('#output-textarea')
  const schemaTextarea = document.querySelector('#schema-textarea')
  const setSchema = document.querySelector('#setschema')
  const setValue = document.querySelector('#setvalue')
  const showErrorsSelect = document.querySelector('#show-errors-select')
  const themeSelect = document.querySelector('#theme-select')
  const themeLink = document.querySelector('#theme-link')
  const validateTextarea = document.querySelector('#validate-textarea')

  const parseUrl = () => {
    const url = window.location.search
    const queryParamsString = url.substring(1, url.length)
    const queryParams = queryParamsString.split('&')
    if (queryParamsString.length) {
      queryParams.forEach((queryParam) => {
        const splittedParam = queryParam.split('=')
        const param = splittedParam[0]
        const value = splittedParam[1]
        if (param === 'data') {
          try {
            data = JSON.parse(LZString.decompressFromBase64(value))
          } catch (reason) {
          }
        }
      })
    }
    mergeOptions()
  }
  const mergeOptions = () => {
    data.options = Object.assign(defaultOptions, data.options)
    refreshUI()
  }
  const refreshUI = () => {
    themeLink.href = 'https://bootswatch.com/4/darkly/bootstrap.min.css'
    iconlibLink.href = 'https://use.fontawesome.com/releases/v5.6.1/css/all.css'
    initJsoneditor()
  }
  const initJsoneditor = () => {
    if (jsoneditor) {
      jsoneditor.destroy()
    }
    jsoneditor = new window.JSONEditor(jsonEditorForm, data.options)
    outputTextarea.value = `" + JsonConvert.SerializeObject(clean, Formatting.Indented).Replace("\\", "\\\\") + @"`
    jsoneditor.setValue(JSON.parse(outputTextarea.value))
    jsoneditor.on('change', updateJson)
    var elem = document.getElementsByTagName('input')
    for(var i = 0; elem[i]; i++) {
        console.log(elem[i]);
        elem[i].addEventListener('input', updateJson)
    }
        updateDirectLink()
  }
function updateJson(e) {
          let json = jsoneditor.getValue()
          outputTextarea.value = JSON.stringify(json, null, 2)

          // validate
          let validationErrors = jsoneditor.validate()
          if (validationErrors.length) {
            validateTextarea.value = JSON.stringify(validationErrors, null, 2)
          } else {
            validateTextarea.value = 'valid'
          }
        }
  const updateDirectLink = () => {
    let url = window.location.href.replace(/\?.*/, '')
    url += '?data='
    url += LZString.compressToBase64(JSON.stringify(data))
    directLink.href = url
  }
  setValue.addEventListener('click', function () {
    jsoneditor.setValue(JSON.parse(outputTextarea.value))
  })
  parseUrl()
</script>
</body></html>";

            //return JsonSchema.FromSampleJson(JsonConvert.SerializeObject(server)).ToJson(Formatting.Indented);
        }
    }
}
